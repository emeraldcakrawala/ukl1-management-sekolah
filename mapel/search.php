<?php

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$cari = $_GET['cari'];
$kategori = $_GET['kategori'];

$query = "SELECT kode_mapel, mapel, alokasi_waktu, semester, nama_guru 
		  FROM matapelajaran LEFT JOIN guru 
		  USING(kode_guru)
		  WHERE $kategori LIKE '%$cari%'
		  ORDER BY kode_mapel";
$result = mysqli_query($connect, $query);
$num = mysqli_num_rows($result);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Cari Data</title>
	<link rel="stylesheet" type="text/css" href="../css/mapel/search.css">
</head>
<body>
	<div class="container">
		<div class="isi">
			<div class="sidebar">
				<div class="sidebar">
					<a href="../guru/gururead.php" class="aside">
						<p class="sidee"><b>Guru</b></p>
					</a>
					<a href="mapelread.php" class="aside">
						<p class="side" id="mapel"><b>Matapelajaran</b></p>
					</a>
					<a href="../home/home.php" class="aside">
						<p class="side"><b>Home</b></p>
					</a>
					<a href="../login/logout.php" class="aside">
						<p class="side"><b>Log Out</b></p>
					</a>
				</div>
			</div>
			<div class="content">
				<h2>DATA MATAPELAJARAN</h2>

				<div class="kotak">
					<div class="kotak1">
						<a href="mapelread.php" class="tampill">
							<button class="tampil">Tampilkan Semua Data Mapel</button>
						</a>
					</div>

					<div class="kotak2">
						<a href="form-create.php" class="tambah" >
							<button class="tambahh">Tambahkan Data</button>
						</a>
					</div>
				</div>

				<table>
					<tr>
						<th class="no">No.</th>
						<th class="kode">Kode Mapel</th>
						<th class="mapel">Matapelajaran</th>
						<th class="waktu">Alokasi Waktu</th>
						<th class="smt">Semester</th>
						<th class="guru">Nama Guru</th>
						<th class="aksi">Aksi</th>
					</tr>

					<?php  
						if($num > 0)
							{
								$no = 1;
								while ($data =  mysqli_fetch_assoc($result)) 
								{ ?>

									<tr>
										<td> <?php echo $no; ?> </td>
										<td> <?php echo $data['kode_mapel'] ?> </td>
										<td> <?php echo $data['mapel'] ?> </td>
										<td> <?php echo $data['alokasi_waktu'] ?> </td>
										<td> <?php echo $data['semester'] ?> </td>
										<td> <?php 
											if($data['nama_guru'] != NULL)
												{
													echo $data['nama_guru'];	
												}
								else 
									{
										echo "-";
									}
										?> 
										</td>
										<td>
											<a href="form-update.php?kode_mapel=<?php echo $data['kode_mapel']; ?>"> Edit | </a>
											<a href="delete.php?kode_mapel=<?php echo $data['kode_mapel']; ?>" onclick="return confirm('Anda Yakin Ingin Menghapus Data?')"> Hapus</a>
										</td>
									</tr>	

									<?php 
									$no++;
								}
							}

								else 
									{
										echo "<tr><td colspan='7'> Tidak Ada Data </td></tr>";
									}
									?>
				</table>
			</div>
		</div>
	</div>
</body>
</html>