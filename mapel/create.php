<?php 

if (!(isset($_POST['btnSimpan']))) 
{
	header("location: form-create.php");
}

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$kode_mapel = $_POST['kode_mapel'];
$mapel = $_POST['mapel'];
$alokasi_waktu = $_POST['alokasi_waktu'];
$semester = $_POST['semester'];
$kode_guru = $_POST['kode_guru'];

$query = "INSERT INTO matapelajaran
		  VALUES ('$kode_mapel', '$mapel', '$alokasi_waktu', '$semester', '$kode_guru')";

$result = mysqli_query($connect, $query);

$num = mysqli_affected_rows($connect);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Tambah Data</title>
	<link rel="stylesheet" type="text/css" href="../css/mapel/create.css">
</head>
<body>
	<div class="container">
		<div class="isi">
			<?php 	
				if($num > 0)
				{
					echo("<p><b>Berhasil Menambahkan Data</b></p>");
				}
				else 
				{
					echo("<p><b>Gagal Menambahkan Data</b></p>");
				}
				echo "<a href = 'mapelread.php'> Lihat Data </a>";
			?>
		</div>
	</div>
</body>
</html>