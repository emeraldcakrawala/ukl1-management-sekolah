<?php  

if(!(isset($_GET['kode_mapel'])))
{
	header("location: mapelread.php");
}

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$kode_mapel = $_GET['kode_mapel'];

$query = "SELECT kode_mapel, mapel, alokasi_waktu, semester, matapelajaran.kode_guru, nama_guru 
		  FROM matapelajaran LEFT JOIN guru	
		  USING(kode_guru)
		  WHERE kode_mapel = '$kode_mapel'";

$result = mysqli_query($connect, $query);
$data_mapel = mysqli_fetch_assoc($result);
	
?>

<!DOCTYPE html>
<html>
<head>
	<title>Ubah Data</title>
	<link rel="stylesheet" type="text/css" href="../css/mapel/form-update.css">
</head>
<body>
	<div class="container">
		<div class="isi">
			<div class="isi1">
				<img src="../gambar/books.png">
				<p><b>Ubah Data Mapel</b></p>
				<p id="nama"><b>UK.SCHOOL</b></p>
			</div>
			<div class="isi2">
				<form class="" action="update.php" method="POST">
							
					<p><label for="kode_mapel"><b>Kode Mapel</b></label></p>
					<input type="text" name="kode_mapel" id="kode" readonly value="<?php echo$data_mapel['kode_mapel']; ?>" class="input1">

					<p><label for="mapel"><b>Matapelajaran</b></label></p>
					<input type="text" name="mapel" value="<?php echo$data_mapel['mapel']; ?>" class="input1">

					<p><label for="alokasi_waktu"><b>ALokasi Waktu</b></label></p>
					<input type="text" name="alokasi_waktu" value="<?php echo$data_mapel['alokasi_waktu']; ?>" class="input1">

					<p><label for="semester"><b>Semester</b></label></p>
					<input type="text" name="semester" value="<?php echo$data_mapel['semester']; ?>" class="input1">

					<p><label for="nama_guru"><b>Guru Pengajar</b></label></p>
						<select name="kode_guru" id="nama_guru" class="input3">
							<option value="-">Tidak Ada Pengajar</option>
								<?php 
									$query2 = "SELECT kode_guru, nama_guru FROM guru";
									$result2 = mysqli_query($connect, $query2);
									while ($data = mysqli_fetch_assoc($result2)) { ?>
									<option value = "<?php echo $data['kode_guru']; ?>" 
										<?php if($data_mapel['kode_guru'] == $data['kode_guru']) {echo "selected";} ?>> 
										<?php echo $data['nama_guru']; ?>
									</option>
								<?php
									}
								?>
						</select>
							
					<input type="submit" name="btnSimpan" id="input2">
				</form>
			</div>
		</div>
	</div>
</body>
</html>