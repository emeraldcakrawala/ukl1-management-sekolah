<?php 

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$query = "SELECT kode_guru, nama_guru FROM guru";
$result = mysqli_query($connect, $query);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Tambah Data</title>
	<link rel="stylesheet" type="text/css" href="../css/mapel/form-create.css">
	<script type="" src="../validasi/validasijsmapel.js"></script>
</head>
<body>
	<div class="container">
		<img src="../gambar/nama2.png">
		<div class="isi">
			<h1>Tambah Data Matapelajaran</h1>
				<form class="" action="create.php" method="POST" onsubmit="return validatejsmapel()">
					<input class="inputt" type="text" name="kode_mapel" placeholder="Kode Mapel" id="kode_mapel">
					<input class="inputt" type="text" name="mapel" placeholder="Matapelajaran" id="mapel">

					<input class="inputt" type="number" name="alokasi_waktu" placeholder="Alokasi Waktu" id="alokasi_waktu">
					<input class="inputt" type="number" name="semester" placeholder="Semester" id="semester">
					
					<select class="inputtt" name="kode_guru" id="kode_guru" id="nama_guru">
						<option value="NULL" id="nama_guru">Nama Guru</option>
							<?php 
								while ($data = mysqli_fetch_assoc($result)) { ?>
									<option value = "<?php echo $data['kode_guru']; ?>">
										<?php echo $data['nama_guru']; ?>
									</option>
							<?php
								}
							?>
					</select>

					<input type="submit" name="btnSimpan" id="button">
				</form>
		</div>
	</div>
</body>
</html>


