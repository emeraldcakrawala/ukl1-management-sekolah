<?php 

if (!(isset($_POST['btnSimpan']))) 
{
	header("location: form-update.php");
}

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$kode_mapel = $_POST['kode_mapel'];
$mapel = $_POST['mapel'];
$alokasi_waktu = $_POST['alokasi_waktu'];
$semester = $_POST['semester'];
$kode_guru = $_POST['kode_guru'];

$query = "UPDATE matapelajaran SET mapel = '$mapel',
								alokasi_waktu = '$alokasi_waktu',
								semester = '$semester',
								kode_guru = '$kode_guru'
		  WHERE kode_mapel = '$kode_mapel'";

$result = mysqli_query($connect, $query);
$num = mysqli_affected_rows($connect);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Ubah Data</title>
	<link rel="stylesheet" type="text/css" href="../css/mapel/update.css">
</head>
<body>
	<div class="container">
		<div class="isi">
			<?php 

				if($num > 0)
				{
					echo("<p><b>Berhasil Mengubah Data</b></p>");
				}
				else 
				{
					echo("<p><b>Gagal Mengubah Data</b></p>");
				}
				echo "<a href = 'mapelread.php'>  Lihat Data </a>";
				
			?>
		</div>
	</div>
</body>
</html>