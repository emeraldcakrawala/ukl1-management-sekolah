<?php

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$query = "SELECT kode_mapel, mapel, alokasi_waktu, semester, nama_guru 
		  FROM matapelajaran LEFT JOIN guru 
		  USING(kode_guru)
		  ORDER BY kode_mapel";
$result = mysqli_query($connect, $query);
$num = mysqli_num_rows($result);

$username = $_SESSION['user'];
$query2 = "SELECT * FROM user WHERE username == '$username'";
$result2 = mysqli_query($connect, $query);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Mata Pelajaran</title>
	<link rel="stylesheet" type="text/css" href="../css/mapel/read.css">
	<script type="" src="../validasi/validasicari.js"></script>
</head>
<body>
	<div class="container">
		<div class="isi">
			<div class="sidebar">
				<div class="sidebar">
					<a href="../guru/gururead.php" class="aside">
						<p class="sidee"><b>Guru</b></p>
					</a>
					<a href="mapelread.php" class="aside">
						<p class="side" id="mapel"><b>Matapelajaran</b></p>
					</a>
					<a href="../login/logout.php" class="aside">
						<p class="side"><b>Log Out</b></p>
					</a>
					<p class="side" id="mapell"><b>Halo, <?php echo $username?>!</b></p>
				</div>
			</div>
			<div class="content">
				<h2>DATA MATAPELAJARAN</h2>

				<div class="kotak">
					<div class="kotak1">
						<form action="search.php" method="get" onsubmit="return validasipencarian()">
							<input id="input1" type="search" name="cari" placeholder="Masukkan Pencarian..">
								<select id="input3" name="kategori">
									<option value="kode_mapel">Kode Mapel</option>
									<option value="mapel">Matapelajaran</option>
									<option value="alokasi_waktu">Alokasi Waktu</option>
								</select>
							<input id="input2" type="submit" name="" value="Cari">
						</form>
					</div>

					<div class="kotak2">
						<a href="form-create.php" class="tambah" >
							<button class="tambahh">Tambahkan Data</button>
						</a>
					</div>
				</div>

				<table>
					<tr>
						<th class="no">No.</th>
						<th class="kode">Kode Mapel</th>
						<th class="mapel">Matapelajaran</th>
						<th class="waktu">Alokasi Waktu</th>
						<th class="smt">Semester</th>
						<th class="guru">Nama Guru</th>
						<th class="aksi">Aksi</th>
					</tr>

					<?php  
						if($num > 0)
							{
								$no = 1;
								while ($data =  mysqli_fetch_assoc($result)) 
								{ ?>

									<tr>
										<td> <?php echo $no; ?> </td>
										<td> <?php echo $data['kode_mapel'] ?> </td>
										<td> <?php echo $data['mapel'] ?> </td>
										<td> <?php echo $data['alokasi_waktu'] ?> </td>
										<td> <?php echo $data['semester'] ?> </td>
										<td> <?php 
											if($data['nama_guru'] != NULL)
												{
													echo $data['nama_guru'];	
												}
								else 
									{
										echo "-";
									}
										?> 
										</td>
										<td>
											<a href="form-update.php?kode_mapel=<?php echo $data['kode_mapel']; ?>"> Edit | </a>
											<a href="delete.php?kode_mapel=<?php echo $data['kode_mapel']; ?>" onclick="return confirm('Anda Yakin Ingin Menghapus Data?')"> Hapus</a>
										</td>
									</tr>	

									<?php 
									$no++;
								}
							}

								else 
									{
										echo "<tr><td colspan='7'> Tidak Ada Data </td></tr>";
									}
									?>
				</table>
			</div>
		</div>
	</div>
</body>
</html>