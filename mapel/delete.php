<?php  

if(!(isset($_GET['kode_mapel'])))
{
	header("location: mapelread.php");
}

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$kode_mapel = $_GET['kode_mapel'];
$query = "DELETE FROM matapelajaran WHERE kode_mapel = '$kode_mapel'";
$result = mysqli_query($connect, $query);
$num = mysqli_affected_rows($connect);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Hapus Data</title>
	<link rel="stylesheet" type="text/css" href="../css/mapel/delete.css">
</head>
<body>
	<div class="container">
		<div class="isi">
			<?php 
				if($num > 0) 
				{
					echo "<p><b>Berhasil Menghapus Data <br></b></p>";
				}
				else
				{
					echo "<p><b>Gagal Menghapus Data<br></b></p>";
				}

				echo "<a href='mapelread.php'>Lihat Data</a>";
			?>
		</div>
	</div>
</body>
</html>