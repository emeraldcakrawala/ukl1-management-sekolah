function validatejsmapel() {
	var kode_mapel = document.getElementById('kode_mapel');
	var mapel = document.getElementById('mapel');
	var alokasi_waktu = document.getElementById('alokasi_waktu');
	var semester = document.getElementById('semester');
	var nama_guru = document.getElementById('nama_guru');

	if (kode_mapel.value == "") {
		alert("Anda Belum Menginputkan Kode");
		kode_mapel.focus();
		return false;
	}

	else if (mapel.value == "") {
		alert("Anda Belum Menginputkan Matapelajaran");
		mapel.focus();
		return false;
	}

	else if (alokasi_waktu.value == "") {
		alert("Anda Belum Menginputkan Alokasi Waktu");
		alokasi_waktu.focus();
		return false;
	}

	else if (semester.value == "") {
		alert("Anda Belum Menginputkan Semester");
		semester.focus();
		return false;
	}


	else if (nama_guru.value == "-") {
		alert("Anda Belum Memilih Nama Guru");
		nama_guru.focus();
		return false;
	}

	else {
		return true;
	}
}