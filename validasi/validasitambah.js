function validatetambah() {
	var nama_guru = document.getElementById('nama_guru');
	var jumlah_jam = document.getElementById('jumlah_jam');
	var alamat = document.getElementById('alamat');
	var telp = document.getElementById('telp');
	var email = document.getElementById('email');

	if (nama_guru.value == "") {
		alert("Anda Belum Menginputkan Nama Guru");
		nama_guru.focus();
		return false;
	}

	else if (jumlah_jam.value == "") {
		alert("Anda Belum Menginputkan Jumlah Jam");
		jumlah_jam.focus();
		return false;
	}

	else if (alamat.value == "") {
		alert("Anda Belum Menginputkan Alamat");
		alamat.focus();
		return false;
	}

	else if (telp.value == "") {
		alert("Anda Belum Menginputkan Telp");
		telp.focus();
		return false;
	}

	else if (email.value == "") {
		alert("Anda Belum Menginputkan Email");
		email.focus();
		return false;
	}

	else {
		return true;
	}
}