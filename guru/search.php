<?php 

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$cari = $_GET['cari'];
$kategori = $_GET['kategori'];

$query = " SELECT * FROM guru
		   WHERE $kategori LIKE '%$cari%'
		   ORDER BY kode_guru";

$result = mysqli_query($connect, $query);
$num = mysqli_num_rows($result);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Guru</title>
	<link rel="stylesheet" type="text/css" href="../css/guru/search.css">
</head>
<body>
	<div class="container">
		<div class="isi">
			<div class="sidebar">
				<a href="gururead.php" class="aside">
					<p class="sidee" id="guruu"><b>Guru</b></p>
				</a>
				<a href="../mapel/mapelread.php" class="aside">
					<p class="side"><b>Matapelajaran</b></p>
				</a>
				<a href="../home/home.php" class="aside">
					<p class="side"><b>Home</b></p>
				</a>
				<a href="../login/logout.php" class="aside">
					<p class="side"><b>Log Out</b></p>
				</a>
			</div>

			<div class="content">

				<h2>DATA GURU</h2>
				
				<div class="kotak">
					<div class="kotak1">
						<a href="gururead.php" class="tampill">
							<button class="tampil">Tampilkan Semua Data Guru</button>
						</a>
					</div>

					<div class="kotak2">
						<a href="form-create.php" class="tambah">
							<button class="tambahh">Tambahkan Data</button>
						</a>
					</div>
				</div>

				<table>
					<tr>
						<th class="no">No.</th>
						<th class="kode">Kode Guru</th>
						<th class="nama">Nama Guru</th>
						<th class="jam">Jam Mengajar</th>
						<th class="alamat">Alamat</th>
						<th class="telp">Telepon</th>
						<th class="email">Email</th>
						<th class="aksi">Aksi</th>
					</tr>

					<?php
						if($num > 0)
							{
								$no = 1;
								while ($data = mysqli_fetch_assoc($result)) 
									{ 
										echo "<tr>";
										echo "<td>" . $no . "</td>";
										echo "<td>" . $data['kode_guru'] . "</td>";
										echo "<td>" . $data['nama_guru'] . "</td>";
										echo "<td>" . $data['jumlah_jam'] . "</td>";
										echo "<td>" . $data['alamat'] . "</td>";
										echo "<td>" . $data['telp'] . "</td>";
										echo "<td>" . $data['email'] . "</td>";
										echo "<td><a href='form-update.php?kode_guru=" . $data['kode_guru'] . "'>Edit</a> |";
										echo "<a href='delete.php?kode_guru=" . $data['kode_guru'] . "' onclick='return confirm(\"Apakah Anda Yakin Ingin Menghapus Data?\")'> Hapus</a></td>";
										echo "</tr>";
										$no++;
									}
							}
								else
									{
										echo "<td colspan='8'> Tidak ada data </td>";
									}
					?>
				</table>
			</div>
		</div>
	</div>
</body>
</html>