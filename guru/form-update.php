<?php  

if(!(isset($_GET['kode_guru'])))
{
	header("location: gururead.php");
}

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$kode_guru = $_GET['kode_guru'];

$query = "SELECT * FROM guru WHERE kode_guru = '$kode_guru'";

$result = mysqli_query($connect, $query);

$row = mysqli_fetch_assoc($result);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Ubah Data</title>
	<link rel="stylesheet" type="text/css" href="../css/guru/form-update.css">
</head>
<body>
	<div class="container">
		<div class="isi">
			<div class="isi1">
				<img src="../gambar/presentation.png">
				<p><b>Ubah Data Guru</b></p>
				<p id="nama"><b>UK.SCHOOL</b></p>
			</div>
			<div class="isi2">
				<form class="" action="update.php" method="post">
							
					<p><label for="nama"><b>Nama Guru</b></label></p>
					<input type="text" name="nama_guru" value="<?php echo$row['nama_guru']; ?>" class="input1">

					<p><label for="nama"><b>Jumlah Jam Mengajar</b></label></p>
					<input type="text" name="jumlah_jam" value="<?php echo$row['jumlah_jam']; ?>" class="input1">

					<p><label for="nama"><b>Alamat</b></label></p>
					<input type="text" name="alamat" value="<?php echo$row['alamat']; ?>" class="input1">

					<p><label for="nama"><b>No Telepon</b></label></p>
					<input type="text" name="telp" value="<?php echo$row['telp']; ?>" class="input1">
							
					<p><label for="no_telp" id="telep"><b>Email</b></label></p>
					<input type="text" name="email" value="<?php echo$row['email']; ?>" class="input1">
						
					<input type="hidden" name="kode_guru" value=" <?php echo$row['kode_guru']; ?>">

					<p><input type="submit" name="btnSimpan" value="simpan" id="input2"></p>
				</form>
			</div>
		</div>
	</div>
</body>
</html>