<?php

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Tambah Data Guru</title>
	<link rel="stylesheet" type="text/css" href="../css/guru/form-create.css">
	<script type="" src="../validasi/validasitambah.js"></script>
</head>

<body>
	<div class="container">
		<img src="../gambar/nama2.png">
		<div class="isi">
			<h1>Tambah Data Guru</h1>
			<form class="" action="create.php" method="POST" onsubmit="return validatetambah()">
				<p>
					<input class="inputt" type="text" name="nama_guru" id="nama_guru" placeholder="Nama Guru">
				</p>

				<p>
					<input class="inputt" type="number" name="jumlah_jam" id="jumlah_jam" placeholder="Jumlah Jam Mengajar">
				</p>

				<p>
					<textarea class="inputt" type="text" name="alamat" id="alamat" placeholder="Alamat"></textarea>
				</p>

				<p>
					<input class="inputt" type="text" name="telp" id="telp" placeholder="No Telepon">
				</p>

				<p>
					<input class="inputt" type="email" name="email" id="email" placeholder="Email">
				</p>

				<p>
					<input type="submit" name="btnSimpan" value="Simpan" id="button">
				</p>
			</form>
		</div>
	</div>
</body>
</html>