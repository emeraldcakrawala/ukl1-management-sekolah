<?php  

session_start();
if(!(isset($_SESSION['user'])))
{
	header("location: ../login/form-login.php");
}

include '../connect.php';

$kode_guru = $_GET['kode_guru'];
$query = "DELETE FROM guru WHERE kode_guru = '$kode_guru'";
$result = mysqli_query($connect, $query);
$num = mysqli_affected_rows($connect);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Menghapus Data</title>
	<link rel="stylesheet" type="text/css" href="../css/guru/delete.css">
</head> 
<body>
	<div class="container">
		<div class="isi">
			<?php 
				if($num > 0) 
					{
						echo "<p><b>Berhasil Menghapus Data <br></b></p>";
					}
					else
					{
						echo "<p><b>Gagal Menghapus Data<br></b></p>";
					}

					echo "<a href='gururead.php'>Lihat Data</a>";
			?>
		</div>
	</div>
</body>
</html>